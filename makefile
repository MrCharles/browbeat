
FILES = browbeat browbeat-tmux browbeat-tmux-ls browbeat-tmux-pid browbeat-tmux-running browbeat-pygtk-info

default:
	@echo "\n"
	@echo "Nothing to do."
	@echo "\n"
	@echo "'make install' \t\t will install the binaries in the src/* directory to ~/bin/"
	@echo "'make uninstall' \t will remove those same binaries."
	@echo "~/.config/browbeat \t will be created on first install, but not uninstalled."
	@echo "\t\t\t\t\t any files in this directory are userland, created and managed by the user."
	@echo "\n"

.PHONY: install
install:
	# Directory configuration/install (2)
	mkdir -p ~/.config/browbeat
	chmod 700 ~/.config/browbeat/
	# (1) File install -- browbeat
	cp src/bin/browbeat ~/bin/
	chmod +x ~/bin/browbeat
	chmod og-rwx ~/bin/browbeat
	# (2) File install -- browbeat-tmux
	cp src/bin/browbeat-tmux ~/bin/
	chmod +x ~/bin/browbeat-tmux
	chmod og-rwx ~/bin/browbeat-tmux
	# (3) File install -- browbeat-tmux-ls
	cp src/bin/browbeat-tmux-ls ~/bin/
	chmod +x ~/bin/browbeat-tmux-ls
	chmod og-rwx ~/bin/browbeat-tmux-ls
	# (4) File install -- browbeat-tmux-pid
	cp src/bin/browbeat-tmux-pid ~/bin/
	chmod +x ~/bin/browbeat-tmux-pid
	chmod og-rwx ~/bin/browbeat-tmux-pid
	# (5) File install -- browbeat-tmux-running
	cp src/bin/browbeat-tmux-running ~/bin/
	chmod +x ~/bin/browbeat-tmux-running
	chmod og-rwx ~/bin/browbeat-tmux-running
	# (6) File install -- browbeat-pygtk-info
	cp src/bin/browbeat-pygtk-info ~/bin/
	chmod +x ~/bin/browbeat-pygtk-info
	chmod og-rwx ~/bin/browbeat-pygtk-info
	# (7) File install -- instance.template
	cp src/.config/browbeat/instance.template ~/.config/browbeat/
	chmod -x ~/.config/browbeat/instance.template
	chmod og-rwx ~/.config/browbeat/instance.template
	# (8) File install -- instances.list
	chmod +x util/new-install
	./util/new-install
	chmod -x util/new-install

.PHONY: uninstall
uninstall:
	rm ~/bin/browbeat
	rm ~/bin/browbeat-tmux
	rm ~/bin/browbeat-tmux-ls
	rm ~/bin/browbeat-tmux-pid
	rm ~/bin/browbeat-tmux-running
	rm ~/.config/browbeat/instance.template
	echo "User data file '~/.config/browbeat/instances.list' not removed."

