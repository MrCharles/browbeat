# browbeat

Wherein, browsers behave.

### Definition

**brow &middot; beat** &mdash; _Intimidate into doing something, typically with stern or abusive words._

### Interpretation

I want my web browsers to do what they're told and behave, and I'm doing this _(thus this repo)_ by abusing various processes and shell scripts.

-----
-----

Background:
-----------

In Debian 9 _(Jessie)_, I created some `bash` scripts to manage separate browser sessions.

After upgrading to Debian 10 _(Buster)_, some of those scripts broke, and, knowing it was time to update them, I decided to work on making it less volitale. In addition: track changes and efforts. Especially because things change between browser versions and how they run on _(individual)_ systems.

**Thus this repository.**

-----
-----

Motivations:
------------

I don't like being tracked everywhere, all of the time. In simpler terms, I like being able to laucnh a browser in a _"private"_ or _"incognito"_ session, and not have it communicate any information to a primary. Sans the use of `Tor`. A normal browser will work just fine, but, I'd like to have different browser instances for different intents and purposes, without those bridges or processes sharing information.

-----
-----

Objectives:
-----------

* Launch a browser session, with a temporary user data directory _(and/or profile)_, and have all of that _(temporary)_ trash removed once the session is done _(i.e. the system process, or PID, no longer exists)_.
* Only allow one instance for one purpose _(named, tracked)_.
    * Except for allowing up to 5 or so "about:blank" sessions. _(A secondary objective.)_
* Launch browsers with different parameters for different intents and purposes _(i.e. with or without GPU support, or a wholly offline instance for local operations, or network-limited operations, etc)_.
* Ensure a browser instance _**NEVER**_ migrates to being a child of top-level systemd process id (1).
    * This is a minor problem, especially on multi-user systems _(i.e. Linux)_.

-----
-----

Requirements:
-------------

Built on Debian 10 _(Buster)_, running `#!++` _(CrunchBang PlusPlus)_ **OpenBox**.

* bash _(v5.0.3)_
    * ps &mdash; _standard_
    * wc &mdash; _standard_
    * grep &mdash; _standard_
* tmux _(v2.8)_
* Python3 _(v3.7)_
    * python3-libtmuxp _(via apt)_
    * python3-psutil _(via apt)_

Bash, alongisde standard console commands, are installed by default with Debian/Linux. `tmux`, `python3`, `python3-libtmux`, and `python3-psutil` installed by _(Debian/Linux)_ `apt` _(default)_. **The entire stack and requirements are available via Debian's standard `apt` system!**

-----

