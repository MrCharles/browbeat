#!/usr/bin/env python3

import os
import base64
import zlib

if __name__ == '__main__':
    data = None
    with open("./src/.config/browbeat/instance.template", "rb") as _fh:
        data = _fh.read()
    data = zlib.compress(data, 9)
    data = base64.urlsafe_b64encode(data)
    print(data)
    data = data.decode('utf-8')
    instdata = "INSTDATA=b'"+data[0:80-12]+"\\\n"
    data = data[80-12:]
    while len(data) >= 79:
        instdata += data[0:79]+"\\\n"
        data = data[79:]
    instdata += data+"'"
    print(instdata)



