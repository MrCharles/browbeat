# instance.template

You will find the `instance.template` [here](../src/.config/browbeat/instance.template) in the [../src/.config/](../src/.config/) directory.

During development, this file is injected into the main script, [../src/bin/browbeat](../src/bin/browbeat) using the following:

```bash
$> pwd
$HOME/dev/git/gitlab/browbeat
$> ./util/compress_instance_template.py >> ./src/bin/browbeat
```

The `compress_instance_template.py` script appends to the end of the browbeat file with the bash redirect `>> ./src/bin/browbeat`. Then the main script is edited manually to replace whatever may be existing. The variable name, near the top of the script is `INSTDATA`.

When changed, after _(manually)_ testing, this gets pushed to the browbeat repo.

The [../src/.config/browbeat/instance.template](#) file is not actually used, it is for reference and verification.

After [install](./Install.md), any differences can be verified.

```bash
$> ~/bin/browbeat test instdata >> /tmp/instance.template
$> diff ~/.config/browbeat/instance.template /tmp/instance.template
$> _
```

_This is to say,_ **there should be NO DIFFERENCE** between the test data output from the main python script, and the file installed to the config directory.

If there is a difference, please report the issue, or follow the above methodology to patch things up.

-----
-----

When, or While, making calling a to open a defined browser instance ...

```bash
$> ~/bin/browbeat run chromium-empty
```

The `.config/browbeat/instance.template` **IS NOT USED** _(it is for reference only)_. The compressed version inside the main browbeat script is written, where two variable names are replaced:

* NAME_ME_PLEASE
* COMMAND_ME_PLEASE

These two variables are replaced with the `key:value` pair, as defined in the `~/.config/browbeat/instances.list` file. Where `chromium-empty` is a default entry, the first time browbeat is installed, thus, when the main browbeat script reads the file, `chromium-empty` **is a valid key**, and the **value** is a command that is inserted into the script.

After verifying a valid key and command exists, the script written, it is then given executable permissions, and executed inside a `tmux` environment named `browbeat`.