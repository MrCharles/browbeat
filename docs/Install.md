# browbeat DOCS/Install

-----
-----

**NOTES:**

* This is a user utility, NOT something to be installed on your local system. There is no need or use for sudo. It should install only to your local `$HOME` directory.
* See the [Requirements](./Requirements.md) file.
    * This is designed to run on Debian _(or Ubuntu)_ distributions.
    * This is designed to run with a standard install of `bash` as primary terminal or console, including standard `bash` utilities.
    * This is designed to run GUI browser instances in isolated `tmux`-ified environments _(but not dedicated Virtual Machines or containers)_.
    * This is designed to run on OpenBox.
        * But will likely work for and play nicely with any other GUI as a user sees fit.
        * Any other GUI is up to the individual user to configure _(**manually**)_.
* See the [LICENSE](../LICENSE) ... absolutely NO WARRANTY IS PROVIDED.
    * Read the code.
    * Ask questions if you must.
    * Submit bugs if you find any.
        * Every random whimsical implementation WILL NOT be accomodated.
        * RTFM/RTFC

-----
-----

```bash
$> git clone git@gitlab.com:MrCharles/shmid.git
$> cd shmid
$> make install
$> ~/bin/genshmid --rip
$> ~/bin/getshmid
$> cd ..
$> git clone git@gitlab.com:MrCharles/browbeat.git
$> cd browbeat
$> make install
...
$> ~/bin/browbeat run chromium-empty
True
```

... and a Chromium instance should launch, if you have `Chromium` installed. If you do not have `Chromium` installed, but just-so happen to have `Firefox` installed _(or the `esr` variant)_ ... then, replace the last line with ...

```bash
...
$> ~/bin/browbeat run ff-empty
True
```

These are the [defaults](./Usage_Defaults.md) installed on first-run. If you do not have either of these, but have or prefer another browser, then, see [Usage:Adding Entries](./Usage_AddEntries.md).

-----
-----

### Install Details

Upon first install, the `~/.config/browbeat` directory is created. It has 2 files, `instances.list` and `instance.template`. The latter template file is only for reference. The `instances.list` file is a JSON encoded Python dictionary with the [default entries](./Usage_Defaults.md).

The `instance.template` file may be overwritten upon subsequent versions. It should always match the script written by testing `~/bin/browbeat test instdata`, as designated in [its development file](./Development_InstanceTemplate.md).

The `instances.list` file should not be overwritten after first install. But, for good measure, even though it should be backed-up automatically, you should ensure that you back it up manually.

Otherwise, all of the files going to the local user's `~/bin` directory start with `browbeat`. The main script is simply `~/bin/browbeat`, and subsequent file-names associated with the install are hyphenated, such as `~/bin/browbeat-tmux` &mdash; but that the user should only ever use or reference the main script `~/bin/browbeat`.

The main script, `~/bin/browbeat` actively and regularly seeks the `~/.config/browbeat/instances.list` file. If it is missing any number of errors may be throw, things will not work, and issues may arise.

If you uninstall `browbeat`, the _(configuration file)_ `instance.template` is removed, along with all entries in `~/bin/browbeat-*`. The `~/.config/browbeat/instances.list` file **IS NOT REMOVED** upon uninstall.
