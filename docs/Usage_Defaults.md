# browbeat DOCS/Usage/Defaults

Upon initial install, this software, `browbeat` assumes you have one of either Chromium and/or Firefox installed by default on a Debian Linux distribution.

The default entries in [../src/.config/browbeat/instances.list](../src/.config/browbeat/instances.list), created in or copied into `~/.config/browbeat/instances.list` are as follows:

| key | value |
| --- | ----- |
| chromium-default | "chromium" |
| chromium-empty | "chromium --temp-profile --user-data-dir=$TMPDIR --incognito --new-window" |
| chromium-browbeat-repo | "chromium --disable-gpu --temp-profile --user-data-dir=$TMPDIR --incognito --new-window https://gitlab.com/MrCharles/browbeat" |
| chromium-browbeat-repo-shmid | "chromium --disable-gpu --temp-profile --user-data-dir=$TMPDIR --incognito --new-window https://gitlab.com/MrCharles/shmid" |
| chromium-browbeat-repo-pymntiso | "chromium --disable-gpu --temp-profile --user-data-dir=$TMPDIR --incognito --new-window https://gitlab.com/MrCharles/pymntiso" |
| ff-default | "firefox" |
| ff-empty | "firefox --profile \"$TMPDIR\" --new-instance --private-window" |
| ff-browbeat-repo | "firefox --profile \"$TMPDIR\" --new-instance --private-window https://gitlab.com/MrCharles/browbeat" |
| ff-browbeat-repo-shmid | "firefox --profile \"$TMPDIR\" --new-instance --private-window https://gitlab.com/MrCharles/shmid" |
| ff-browbeat-repo-pymntiso | "firefox --profile \"$TMPDIR\" --new-instance --private-window https://gitlab.com/MrCharles/pymntiso" |

Technically speaking, the key is a quoted string. This file is read and written by python's `json.dumps` and `json.loads` inside of the `src/bin/browbeat -> ~/bin/browbeat` script.

In other words, you have references to a `default` instance, without any parameters, an `empty` instance which is **'incognito'**, and 3 repositories, one of which is this, another is the required `shmid`, and another example, `pymntiso`.

See the [Install](./Install.md) documentation.

**In summary: First you should install** `shmid` **...**

```bash
$ dur/ > git clone git@gitlab.com:MrCharles/shmid.git
$ dur/ > cd shmid
$ dur/shmid/ > make install
...
$ dur/shmid/ > ~/bin/genshmid --rip
$ dur/shmid/ > ~/bin/getshmid
...
$ dur/shmid/ > cat ~/.shmid
...
$ dur/shmid/ > _
```

**Thereafter, install this repo ...**

```bash
$ dur/ > git clone git@gitlab.com:MrCharles/browbeat.git
$ dur/ > cd browbeat
$ dur/browbeat/ > make install
...
$ dur/browbeat/ > _
```

After install, running an instance _(from the command line, as a test)_ is as simple as:

```bash
$ dur/browbeat/ > ~/bin/browbeat run chromium-empty
True
$ dur/browbeat/ > cd $HOME
$ ~/ > ~/bin/browbeat run ff-empty
True
$ ~/ > ls -l /tmp/$(cat ~/.shmid)/running
drwxr-xr-x 14 user user #### MON DD HH:SS $(date +'%s')
-rw-r--r--  1 user user #### MON DD HH:SS chromium-empty
-rwxr-xr-x  1 user user #### MON DD HH:SS chromium-empty.sh
drwxr-xr-x 14 user user #### MON DD HH:SS $(date +'%s')
-rw-r--r--  1 user user #### MON DD HH:SS ff-empty
-rwxr-xr-x  1 user user #### MON DD HH:SS ff-empty.sh
$ ~/ > cat /tmp/$(cat ~/.shmid)/running/chromium-empty
PID
$(date +'%s')
chromium-empty
$ ~/ > cat /tmp/$(cat ~/.shmid)/running/ff-empty
PID
$(date +'%s')
ff-empty
```

If you close one of those windows, then, 3 of those entries should disappear. This is "managed," in the sense of ... the script written to launch an instance should clean-up temporary directories created, and the script itself.

The `browbeat` script is in your local user `bin` directory at `~/bin`, and, asking it to `run` one of `ff-empty` and/or `chromium-empty` should launch either/or both, depending on what you have installed. Thereafter, you can check to see if everything matches up by checking the scripts used to launch, the relative lock files, with the PID of the running processes, etc...

If any of this escapes you, don't ask questions, do some research and learn about `bash`, its scripting, and how to run/launch/manage your file system and binaries and scripts, etc. If after doing research and something is not working as expected or throwing errors from a default install where requirements are met, then please report bugs, issues, and/or ask questions.

