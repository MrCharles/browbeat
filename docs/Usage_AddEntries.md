# browbeat DOCS/Usage/Add Entries

After the initial [Install](./Install.md), setting aside the [Default](./Usage_Defaults.md) entries, you can add new browser instances as such:

-----

**First:**

Create a file that looks like this:

```
browser -parameters URL
```

* This file should have a single line.
* The browser binary should be the first entry on the line.
* Whatever parameters
    * Including a URL if you want or need, depending on the syntax for that particular browser binary, accepted parameters, etc.

-----

**Second:**

You need a unique / individual name to designate or represent this browser instance.

Since it will be stored in and read from a Python dictionary, which is a key-value store, this is the "key," and it should be unique. _(The contents of the above file's first line will be the value; the value does not need to be unique, but this, the name, the key, should be unique.)_

If you get an error or an issue, you can review existing key-names with...

```bash
$> ~/bin/browbeat ls

INSTANCE                        STATUS   PID      COMMAND
chromium-default                stopped           chromium
chromium-empty                  stopped           chromium --temp-profile --user-data-dir=$TMPDIR --incognito --new-window
chromium-browbeat-repo          running     16329 chromium --disable-gpu --temp-profile --user-data-dir=$TMPDIR --incognito --new-window https://gitlab.com/MrCharles/browbeat
chromium-browbeat-repo-shmid    stopped           chromium --disable-gpu --temp-profile --user-data-dir=$TMPDIR --incognito --new-window https://gitlab.com/MrCharles/shmid
chromium-browbeat-repo-pymntiso stopped           chromium --disable-gpu --temp-profile --user-data-dir=$TMPDIR --incognito --new-window https://gitlab.com/MrCharles/pymntiso
ff-default                      stopped           firefox
ff-empty                        stopped           firefox --profile "$TMPDIR" --new-instance --private-window
ff-browbeat-repo                stopped           firefox --profile "$TMPDIR" --new-instance --private-window https://gitlab.com/MrCharles/browbeat
ff-browbeat-repo-shmid          stopped           firefox --profile "$TMPDIR" --new-instance --private-window https://gitlab.com/MrCharles/shmid
ff-browbeat-repo-pymntiso       stopped           firefox --profile "$TMPDIR" --new-instance --private-window https://gitlab.com/MrCharles/pymntiso

```

The `INSTANCE` _(left column)_ are the existing key-names.

-----

**FINALLY:**

To install a new instance...

```bash
$> ~/bin/browbeat add ff-news file-name
```

Where the `ff-news` name is unique to the existing list, and `file-name` is a file with a single line _(which will be read and used as the value for the key)_.

Then, to check that it all works ...

```bash
$> ~/bin/browbeat ls

INSTANCE                        STATUS   PID      COMMAND
chromium-default                stopped           chromium
chromium-empty                  stopped           chromium --temp-profile --user-data-dir=$TMPDIR --incognito --new-window
chromium-browbeat-repo          running     16329 chromium --disable-gpu --temp-profile --user-data-dir=$TMPDIR --incognito --new-window https://gitlab.com/MrCharles/browbeat
chromium-browbeat-repo-shmid    stopped           chromium --disable-gpu --temp-profile --user-data-dir=$TMPDIR --incognito --new-window https://gitlab.com/MrCharles/shmid
chromium-browbeat-repo-pymntiso stopped           chromium --disable-gpu --temp-profile --user-data-dir=$TMPDIR --incognito --new-window https://gitlab.com/MrCharles/pymntiso
ff-default                      stopped           firefox
ff-empty                        stopped           firefox --profile "$TMPDIR" --new-instance --private-window
ff-browbeat-repo                stopped           firefox --profile "$TMPDIR" --new-instance --private-window https://gitlab.com/MrCharles/browbeat
ff-browbeat-repo-shmid          stopped           firefox --profile "$TMPDIR" --new-instance --private-window https://gitlab.com/MrCharles/shmid
ff-browbeat-repo-pymntiso       stopped           firefox --profile "$TMPDIR" --new-instance --private-window https://gitlab.com/MrCharles/pymntiso
ff-news                         stopped           firefox --profile "$TMPDIR" --new-instance --private-window https://news.google.com

```

Notice that there is now a new entry at the end, and, I've simply copy/pasted and modified the `ff-empty` entry with a new name, and appended `https://news.google.com` to the end ...

And, now, when I launch a new instance _(singular, else warned that the instance is already running)_ ...

```bash
$> ~/bin/browbeat run ff-news
True
$> _
$> ~/bin/browbeat ls

INSTANCE                        STATUS   PID      COMMAND
chromium-default                stopped           chromium
chromium-empty                  stopped           chromium --temp-profile --user-data-dir=$TMPDIR --incognito --new-window
chromium-browbeat-repo          running     16329 chromium --disable-gpu --temp-profile --user-data-dir=$TMPDIR --incognito --new-window https://gitlab.com/MrCharles/browbeat
chromium-browbeat-repo-shmid    stopped           chromium --disable-gpu --temp-profile --user-data-dir=$TMPDIR --incognito --new-window https://gitlab.com/MrCharles/shmid
chromium-browbeat-repo-pymntiso stopped           chromium --disable-gpu --temp-profile --user-data-dir=$TMPDIR --incognito --new-window https://gitlab.com/MrCharles/pymntiso
ff-default                      stopped           firefox
ff-empty                        stopped           firefox --profile "$TMPDIR" --new-instance --private-window
ff-browbeat-repo                stopped           firefox --profile "$TMPDIR" --new-instance --private-window https://gitlab.com/MrCharles/browbeat
ff-browbeat-repo-shmid          stopped           firefox --profile "$TMPDIR" --new-instance --private-window https://gitlab.com/MrCharles/shmid
ff-browbeat-repo-pymntiso       stopped           firefox --profile "$TMPDIR" --new-instance --private-window https://gitlab.com/MrCharles/pymntiso
ff-news                         running      6845 firefox --profile "$TMPDIR" --new-instance --private-window https://news.google.com

```

And now the `ff-news` instance is running.

-----

