# browbeat DOCS/Readme

* Development
    * [instance.template](Development_InstanceTemplate.md)
    * [#!/usr/bin/env bash](Development_UsrBinEnv.md)
* [Install](./Install.md)
* Usage
    * [Defaults](./Usage_Defaults.md)
    * [Adding Entries](./Usage_AddEntries.md)
        * [RESERVED!!!](./RESERVED.md)
* Bugfixes
    * [Bash History Truncated](./bugfixes/Bash_History_Truncated.md) &mdash; 16-October-2019

