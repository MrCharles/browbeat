# Bash History Truncated

**Fixed:** 16-October-2019

**Issue:** Tmux was misbehaving. Unknown source _(tmux docs suck)_. Instead of using my default bash settings, as designated in `proilfe` and `rc` files, my home directory bash history file, `~/.bash_history` was being truncated to 500 lines. Attaching to the `browbeat` tmux session, `echo $HISTFILESIZE` gave 500. So, there was the problem.

**Solution:** In the `~/bin/browbeat-tmux` file _(bash script)_, added the following lines before launching the browbeat tmux session:

```bash
export HISTSIZE=1000
export HISTFILESIZE=1000
export HISTFILE=~/.tmux_history
```

This solved the issue. Tmux console history is now written to a separate file, and truncates at 1000 lines of history.

-----

This was a very irritating issue. In part, the "fix," and in part the loss of my default bash history. Stupid thing should have used my default bash settings _(nothing about environment modified prior to launching tmux sessions)_ and not arbitrarily truncated my bash file. Just because tmux is special, doesn't mean it should screw with default environment settings and change base or core environment. Things like this should be better documented, and default to a user's home environment preferences. Tmux should not truncate a user's bash history file prior to exceeding user-defined behavior, not tmux defined behavior, or system defaults, or whatever the "500" case was. _(That was not my intent, and I still can't find the documentation that explains this behavior, and it pisses me off something fierce.)_