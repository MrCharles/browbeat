# Development: `#!/usr/bin/env bash`

The start of the bash scripts in the [../src](../src) directory start with...

```bash
#!/usr/bin/env bash
```

This is opposed to other _(executable)_ bash scripts often having something like...

```bash
#!/bin/bash
```

The reason for using `/usr/bin/env bash` is to setup a temporary environment where variables can be modified without harming global user scope _(that is, a user's base or core login session)_.

In the session, with the temporary environment defined, core values like `$USER` and `$HOME` are re-defined. Once the session ends, these changes are disposed and do not negatively effect any other session _(or scope thereabouts)_.

Other unnecessary or irrelevant values are removed. For example, from a `bash` prompt, you could do the following:

```bash
terminal $> env
...
...
...
...
...
terminal $> env -i bash
newenv $> env
....
.....
......
newenv $> exit
terminal $> _
```

However, `bash` _scripts_ don't always like or play nice with passing paramters on the `#!` line. **Notice** that where there were 5x `...` lines in your basic terminal environment, the `-i` flag being sent to a new command removed 2 of those, and others may be modified.

The following entries are preserved:

* USER=$USER
* HOME=/home/$USER
* PATH=$HOME/bin:/usr/local/bin:/usr/bin:/bin
* DISPLAY=:0
* _=/usr/bin/env

The `DISPLAY` variable is necessary, whatever it is, for being able to launch a browser to the local display.

The `USER` and `HOME` variables are respectively preserved at `OUSER` and `OHOME` _(for Original)_, and the temporary environment variables are redefined.

The `PATH` is necessary for the bash script to continue operating as expected _(i.e. the environment needs to know where its commands reside)_. If you take away its knowledge of commands via `unset` &mdash; then it starts panicking and throwing errors about not being able to find standard bash commands like `ps` or `grep`, etc...

After re-defining `USER` and `HOME` and making those basic directories, then the script can launch/execute a browser instance, and it is non-the-wiser that it is not being run from the individual user's home directory.

-----
-----

### Notes/Erratta

This only works as long as executable scripts don't seek knowledge of the user executing the binary to launch a process. At present, most of them rely upon standard system variables. But, they could be re-worked to seek information about the individual user launching the process, simply read and replace standard system information, irrespective. _(Except for the fact this would lead to extremely bad PR.)_

**Important Review:**

* about:about
* **FireFox**
    * about:support
    * about:profiles
* **Chromium**
    * about:version

If you review the above resources, they should look mostly clean _(not have references to you or your profile)_. This is not to say that the binaries aren't doing stuff behind the scenes, but, if you use this persistently and ensure it works properly from time-to-time any browser stuff in your local user profile should never have file or date changes. _(Meaning, if you're sure that completely independent browser instances should not be interacting with your local user profile, then the browser is misbehaving and disregarding local user or systems administrator management; bad PR.)_
