# Eratta

-----

### Issue #0

1. This is user-land only. _(Not a package to be installed on the local system for all users.)_
2. If you launch FireFox, you'll notice the `about:profiles` page lists profiles in the user's home directory.
    * If you go to `about:support` &mdash; here the temporary profile directory is listed.
    * So, obviously your browser can still be profiled.
3. The same holds in various capacities for Chromium, too.
4. Even Tor can be profiled.
    * See the _"if you maximize your window..."_ warning.
5. Subsequent and/or future versions of this software may be entirely different.
    * Looking for a workaround, such as a minimal chroot'ed system (jailed) with temporary docker containers on top _**(???)**_

Point being: This project is minimally useful to a certain, reasonable degree. It does not guarantee any security or absolute privacy, but it does limit nuisance advertising and collection of collated data. _(Puts it off on the companies collecting data to collate that data, instead of making it too easy by persistently storing it in a user's home directory, etc.)_

-----
-----

### Issue #1

To add a browser instance to the managed list:

```bash
$> ~/bin/browbeat add my-name ~/path/to/file_with_single_line
```

The **first line** of the file located at `/home/$USER/path/to/file_with_single_line` will be read _(and not tested)_ as the browser, with all necessary parameters, to launch. See **Issue #2** _(below)_ for the managed list/dictionary format &mdash; also, don't add I/O redirecton or multiple commands on the same line with `&` or `&&` &mdash; as this will result in unexpected behavior.

If there is a problem, remove the entry from the list.

This can be done by...

```bash
$> ~/bin/browbeat rm my-name
```

-----
-----

### Issue #2

When adding a new browser instance, I/O redirection should not be added. It will be added at runtime.

**Here is the default dictionary:**

```python
>>> {"chromium-empty": "chromium --temp-profile --user-data-dir=$TMPDIR --incognito --new-window",
    "chromium-about-blank": "chromium --temp-profile --user-data-dir=$TMPDIR --incognito --new-window about:blank",
    "chromium-repo": "chromium --disable-gpu --temp-profile --user-data-dir=$TMPDIR --incognito --new-window https://gitlab.com/MrCharles/browbeat"}
```

**When browbeat is called...**

```bash
$> ~/bin/browbeat run chromium-empty
True
$> _
```

_... the key's value has the following appended to it ..._

```bash
# Raw Dict Value for key chromium-empty
#chromium --temp-profile --user-data-dir=$TMPDIR --incognito --new-window
# Modified value with I/O redirection appended.
chromium --temp-profile --user-data-dir=$TMPDIR --incognito --new-window &>/dev/null & # Dict
```

In the process of adding a new browser instance to the managed list, if the string representing the browser instance to launch has a `&` character, the string is split on this character, and only the first member `[0]` is written into the value for a new key, which may result in unexpected behavior.

In other words, **it should be a single command, on one line.**

If you want or need many commands, point to a script instead of browser instance, and have the script run that browser instance as the very last command. This, however, may not work, as the next line is `PID=$!`, meant to grab the PID of the last executed command.

It might be more comfortable for some users to add entries manually: `~/.config/browbeat/instances.list` &mdash; it should read exactly like a Python dictionary of keys and values, with `json.loads` and `json.dumps` operating normally _(casted to/from in the core `browbeat` script)_.

-----
-----