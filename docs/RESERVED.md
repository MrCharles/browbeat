# docs/Usage/RESERVED

Following [Adding Entries](./Usage_AddEntries.md), there are **TWO** (2) reserved entries for the named keys, and they are:

* tor
* tor-browser

If you attempt to use either of these entries without first reading the documentation and/or code, things will fail or break.

However, you could use any of ...

* Tor
* Tor-browser
* Tor...
* tor-whatever
* ...

... because keys in a Python dictionary are case-sensitive.

-----

### Simply put:

The developer installs a local `Tor` [browser](https://www.torproject.org/) instance at `~/bin/tor/...` which gets copied over into a temporary directory with `rsync -ar` before launch. It is an exception which can be reviewed in the `src/.config/browbeat/instance.template` or `~/.config/browbeat/instance.template` file(s). Notably, if you work this out for yourself, `Tor` is based on `FireFox`, and uses the same _(launch)_ parameters _(obviously; sans version differences or separation)_.

-----

No support or assistance will be rendered. RTFM or RTFC _(Read the F*ing Manual/Code)_.

You may use any other key-name, such as `tor-whatever` with a locally installed binary _(like any other browser, such as Safari or Opera, or whatever)_. If you use a named path to launch something like this, expect it to refer back to your home or install directory, that of the UID _(User ID)_ calling a named path _(i.e. not fully obscured)_. Such that file-writes to the base/core path or directory used to call a `Tor` _(or any other browser)_ binary, or your user's home directory, would be an expected result if you go this route. This is despite efforts to obscure such with `#!/usr/bin/env bash` ...

